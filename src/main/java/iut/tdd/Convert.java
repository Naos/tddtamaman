package iut.tdd;

import java.util.HashMap;

public class Convert {
	
	private static HashMap<String ,String > zeroToSeize;
	
	final private static String ZERO = "zéro";
	final private static String UN = "un";
	final private static String DEUX = "deux";
	final private static String TROIS = "trois";
	final private static String QUATRE = "quatre";
	final private static String CINQ = "cinq";
	final private static String SIX = "six";
	final private static String SEPT = "sept";
	final private static String HUIT = "huit";
	final private static String NEUF = "neuf";
	final private static String DIX = "dix";
	final private static String ONZE = "onze";
	final private static String DOUZE = "douze";
	final private static String TREIZE = "treize";
	final private static String QUATORZE = "quatorze";
	final private static String QUINZE = "quinze";
	final private static String SEIZE = "seize";
	
	final private static String VINGT = "vingt";
	final private static String TRENTE = "trente";
	final private static String QUARANTE = "quarante";
	final private static String CINQUANTE = "cinquante";
	final private static String SOIXANTE = "soixante";
	final private static String SOIXANTE_DIX = "soixante_dix";
	final private static String QUATRE_VINGT = "quatre_vingt";
	final private static String QUATRE_VINGT_DIX = "quatre_vingt_dix";
	
	final private static String CENT = "cent";
	final private static String MILLE = "mille";
	final private static String MILLIONS = "millions";
	final private static String MILLIARDS = "milliards";
	
	
	
	public static String num2text(String input) {
		
		zeroToSeize = new HashMap<String, String>();
		
		zeroToSeize.put("0", ZERO);
		zeroToSeize.put("1", UN);
		zeroToSeize.put("2", DEUX);
		zeroToSeize.put("3", TROIS);
		zeroToSeize.put("4", QUATRE);
		zeroToSeize.put("5", CINQ);
		zeroToSeize.put("6", SIX);
		zeroToSeize.put("7", SEPT);
		zeroToSeize.put("8", HUIT);
		zeroToSeize.put("9", NEUF);
		zeroToSeize.put("10", DIX);
		zeroToSeize.put("11", ONZE);
		zeroToSeize.put("12", DOUZE);
		zeroToSeize.put("13", TREIZE);
		zeroToSeize.put("14", QUATORZE);
		zeroToSeize.put("15", QUINZE);
		zeroToSeize.put("16", SEIZE);
		
		zeroToSeize.put("20", VINGT);
		zeroToSeize.put("30", TRENTE);
		zeroToSeize.put("40", QUARANTE);
		zeroToSeize.put("50", CINQUANTE);
		zeroToSeize.put("60", SOIXANTE);
		zeroToSeize.put("70", SOIXANTE_DIX);
		zeroToSeize.put("80", QUATRE_VINGT);
		zeroToSeize.put("90", QUATRE_VINGT_DIX);
		
		zeroToSeize.put("100", CENT);
		zeroToSeize.put("1000", MILLE);
		zeroToSeize.put("1000000", MILLIONS);
		zeroToSeize.put("1000000000", MILLIARDS);
		
		
		
		
		
		return zeroToSeize.get(input);
	}
	
	public static String text2num(String input) {
		
		return null;
	}
}